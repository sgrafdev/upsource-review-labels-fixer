# Upsource Review Labels Fixer

## Requirements

* Requires node 8 or more recent.
* Requires credentials for an Upsource account with admin permissions.

## Installation
`npm install`

## Configuration

Edit the file configuration.json to configure the script.

1. Enter address and admin user credentials for Upsource API.
2. Enter project ids of projects which should not be changed.
3. Enter a list of custom labels to create.

The format for a label is:
`{
    "name": "Best Practices",
    "colorId": "0"
}`
name is the label text, colorId is the position of the color in Upsource's predefined colors (ranging from 0 to 15)

## Execution
`npm run`