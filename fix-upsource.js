/**
 * Connecto to upsource API to standardize review labels.
 *
 * First run the commande `npm install` to install dependencies.
 * Then run this file with the command `npm start`.
 */
const fetch = require("node-fetch");
const btoa = require("btoa");
const _ = require("lodash");

const configuration = require("./configuration.json");

const UPSOURCE_API_PREFIX = configuration.upsource_address;
const UPSOURCE_USERNAME = configuration.upsource_username;
const UPSOURCE_PASSWORD = configuration.upsource_password;

// project id of project we want to skip
const SKIP_PROJECTS = configuration.skip_projects;

// labels we want to create
const CUSTOM_LABELS = configuration.labels;

const HEADERS = {
    // basic auth token is a base64 of username:password
    "Authorization": "Basic " + btoa(UPSOURCE_USERNAME + ":" + UPSOURCE_PASSWORD)
};

const fixProjects = async () => {
    try {

        const projects = await getProjects();

        for (let i = 0; i < projects.length; i++) {

            const { projectId, projectName } = projects[i];

            if (SKIP_PROJECTS.indexOf(projectId) > -1) {
                console.log("Skipping " + projectName);
                continue;
            }

            console.log("Fixing " + projectName);

            // get current labels configuration
            const labels = await getLabels(projectId);

            // disable predefined labels if needed
            if (!labels.hidePredefinedLabels) {
                await disablePredefinedLabels(projectId);
            }

            // remove existing custom labels
            await removeExistingLabels(projectId, labels.customLabels);

            // creating the new custom labels
            await createNewLabels(projectId, CUSTOM_LABELS);

            console.log(projectName + " done !");
        }
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
};

const getProjects = () => {
    return fetch(UPSOURCE_API_PREFIX + '/getAllProjects', { headers: HEADERS })
        .then((response) => response.json())
        .then((json) => {
            if (!json || !json.result || !Array.isArray(json.result.project)) {
                throw new Error("Invalid response");
            }
            return json.result.project;
        });
};

const getLabels = (projectId) => {
    const body = JSON.stringify({ projectId });
    return fetch(UPSOURCE_API_PREFIX + '/getReviewLabels', { headers: HEADERS, method: "POST", body })
        .then((response) => response.json())
        .then((json) => {
            if (!json || !json.result) {
                throw new Error("Invalid response");
            }
            return json.result;
        });
};

const disablePredefinedLabels = (projectId) => {
    console.log("Disabling predefined labels");
    const body = JSON.stringify({ projectId, doHide: true });
    return fetch(UPSOURCE_API_PREFIX + '/hidePredefinedReviewLabels', { headers: HEADERS, method: "POST", body })
        .then((response) => response.json())
        .then((json) => {
            if (!json || !json.result) {
                throw new Error("Invalid response");
            }
            console.log("Successfuly disabled predefined labels");
        });
};

const removeExistingLabels = (projectId, labels) => {
    return Promise.all(_.map(labels, (label) => {
        console.log("Deleting existing label " + label.name);
        const body = JSON.stringify({ projectId, label });
        return fetch(UPSOURCE_API_PREFIX + '/deleteReviewLabel', { headers: HEADERS, method: "POST", body })
            .then((response) => response.json())
            .then((json) => {
                if (!json || !json.result) {
                    throw new Error("Invalid response");
                }
                console.log("Successfuly deleted the label " + label.name);
            });
    }));
};

const createNewLabels = (projectId, labels) => {
    return Promise.all(_.map(labels, (label) => {
        console.log("Creating label " + label.name);
        const body = JSON.stringify({ projectId, label });
        return fetch(UPSOURCE_API_PREFIX + '/createOrEditReviewLabel', { headers: HEADERS, method: "POST", body })
            .then((response) => response.json())
            .then((json) => {
                if (!json || !json.result) {
                    throw new Error("Invalid response");
                }
                console.log("Successfuly created the label " + label.name);
            });
    }));
};

// check node version before running
if (Number(process.version.match(/^v(\d+\.\d+)/)[1]) <= 8.0) {
    console.error('node 8+ required');
    process.exit(2);
}

// main entry point
fixProjects();